module.exports = {
  title: 'Hello Demoday',
  base: '/vuepress-demo/',
  dest: 'public',
  themeConfig: {
    nav: [
      { text: 'Home', link: '/' },
      { text: 'Guide', link: '/guide/' },
      { text: 'Config', link: '/config' }
    ],
    docsDir: 'docs',
    docsBranch: 'master'
  }
}